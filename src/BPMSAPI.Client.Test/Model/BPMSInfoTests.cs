/* 
 * BPMS API
 *
 * Универсальный интерфейс взаимодействия с различными BPM системами.
 *
 * OpenAPI spec version: 0.1.3
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 */


using NUnit.Framework;

using System;
using System.Linq;
using System.IO;
using System.Collections.Generic;
using BPMSAPI.Client.Api;
using BPMSAPI.Client.Model;
using BPMSAPI.Client.Client;
using System.Reflection;
using Newtonsoft.Json;

namespace BPMSAPI.Client.Test
{
    /// <summary>
    ///  Class for testing BPMSInfo
    /// </summary>
    /// <remarks>
    /// This file is automatically generated by Swagger Codegen.
    /// Please update the test case below to test the model.
    /// </remarks>
    [TestFixture]
    public class BPMSInfoTests
    {
        // TODO uncomment below to declare an instance variable for BPMSInfo
        //private BPMSInfo instance;

        /// <summary>
        /// Setup before each test
        /// </summary>
        [SetUp]
        public void Init()
        {
            // TODO uncomment below to create an instance of BPMSInfo
            //instance = new BPMSInfo();
        }

        /// <summary>
        /// Clean up after each test
        /// </summary>
        [TearDown]
        public void Cleanup()
        {

        }

        /// <summary>
        /// Test an instance of BPMSInfo
        /// </summary>
        [Test]
        public void BPMSInfoInstanceTest()
        {
            // TODO uncomment below to test "IsInstanceOfType" BPMSInfo
            //Assert.IsInstanceOfType<BPMSInfo> (instance, "variable 'instance' is a BPMSInfo");
        }


        /// <summary>
        /// Test the property 'Type'
        /// </summary>
        [Test]
        public void TypeTest()
        {
            // TODO unit test for the property 'Type'
        }
        /// <summary>
        /// Test the property 'Version'
        /// </summary>
        [Test]
        public void VersionTest()
        {
            // TODO unit test for the property 'Version'
        }
        /// <summary>
        /// Test the property 'Status'
        /// </summary>
        [Test]
        public void StatusTest()
        {
            // TODO unit test for the property 'Status'
        }

    }

}
