/* 
 * BPMS API
 *
 * Универсальный интерфейс взаимодействия с различными BPM системами.
 *
 * OpenAPI spec version: 0.1.3
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 */


using NUnit.Framework;

using System;
using System.Linq;
using System.IO;
using System.Collections.Generic;
using BPMSAPI.Client.Api;
using BPMSAPI.Client.Model;
using BPMSAPI.Client.Client;
using System.Reflection;
using Newtonsoft.Json;

namespace BPMSAPI.Client.Test
{
    /// <summary>
    ///  Class for testing BaseListMeta
    /// </summary>
    /// <remarks>
    /// This file is automatically generated by Swagger Codegen.
    /// Please update the test case below to test the model.
    /// </remarks>
    [TestFixture]
    public class BaseListMetaTests
    {
        // TODO uncomment below to declare an instance variable for BaseListMeta
        //private BaseListMeta instance;

        /// <summary>
        /// Setup before each test
        /// </summary>
        [SetUp]
        public void Init()
        {
            // TODO uncomment below to create an instance of BaseListMeta
            //instance = new BaseListMeta();
        }

        /// <summary>
        /// Clean up after each test
        /// </summary>
        [TearDown]
        public void Cleanup()
        {

        }

        /// <summary>
        /// Test an instance of BaseListMeta
        /// </summary>
        [Test]
        public void BaseListMetaInstanceTest()
        {
            // TODO uncomment below to test "IsInstanceOfType" BaseListMeta
            //Assert.IsInstanceOfType<BaseListMeta> (instance, "variable 'instance' is a BaseListMeta");
        }


        /// <summary>
        /// Test the property 'PageNum'
        /// </summary>
        [Test]
        public void PageNumTest()
        {
            // TODO unit test for the property 'PageNum'
        }
        /// <summary>
        /// Test the property 'PageSize'
        /// </summary>
        [Test]
        public void PageSizeTest()
        {
            // TODO unit test for the property 'PageSize'
        }
        /// <summary>
        /// Test the property 'TotalPages'
        /// </summary>
        [Test]
        public void TotalPagesTest()
        {
            // TODO unit test for the property 'TotalPages'
        }
        /// <summary>
        /// Test the property 'TotalRecordCount'
        /// </summary>
        [Test]
        public void TotalRecordCountTest()
        {
            // TODO unit test for the property 'TotalRecordCount'
        }

    }

}
