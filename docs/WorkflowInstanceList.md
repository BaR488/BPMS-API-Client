# BPMSAPI.Client.Model.WorkflowInstanceList
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Meta** | [**BaseListMeta**](BaseListMeta.md) |  | 
**Items** | [**List&lt;WorkflowInstance&gt;**](WorkflowInstance.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

