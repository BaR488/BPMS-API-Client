# BPMSAPI.Client.Api.WorkflowInstanceVariablesApi

All URIs are relative to *http://localhost:5000/api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**AddEngineWorkflowInstanceVariable**](WorkflowInstanceVariablesApi.md#addengineworkflowinstancevariable) | **POST** /workflows/{workflowId}/instances/{instanceId}/variables | Добавление переменной в экземпляр процесса.
[**GetEngineWorkflowInstanceVariableValue**](WorkflowInstanceVariablesApi.md#getengineworkflowinstancevariablevalue) | **GET** /workflows/{workflowId}/instances/{instanceId}/variables/{variableId} | Получение значения переменной экземпляра процесса.
[**GetEngineWorkflowInstanceVariables**](WorkflowInstanceVariablesApi.md#getengineworkflowinstancevariables) | **GET** /workflows/{workflowId}/instances/{instanceId}/variables | Получение списка переменных экземпляра процесса.
[**SetEngineWorkflowInstanceVariable**](WorkflowInstanceVariablesApi.md#setengineworkflowinstancevariable) | **PUT** /workflows/{workflowId}/instances/{instanceId}/variables/{variableId} | Изменение переменной в экземпляре процесса.


<a name="addengineworkflowinstancevariable"></a>
# **AddEngineWorkflowInstanceVariable**
> void AddEngineWorkflowInstanceVariable (string workflowId, string instanceId, WorkflowInstanceVariable workflowInstanceVariable)

Добавление переменной в экземпляр процесса.

### Example
```csharp
using System;
using System.Diagnostics;
using BPMSAPI.Client.Api;
using BPMSAPI.Client.Client;
using BPMSAPI.Client.Model;

namespace Example
{
    public class AddEngineWorkflowInstanceVariableExample
    {
        public void main()
        {
            var apiInstance = new WorkflowInstanceVariablesApi();
            var workflowId = workflowId_example;  // string | Идентификатор процесса
            var instanceId = instanceId_example;  // string | Идентификатор экземпляра процесса
            var workflowInstanceVariable = new WorkflowInstanceVariable(); // WorkflowInstanceVariable | Переменная процесса

            try
            {
                // Добавление переменной в экземпляр процесса.
                apiInstance.AddEngineWorkflowInstanceVariable(workflowId, instanceId, workflowInstanceVariable);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling WorkflowInstanceVariablesApi.AddEngineWorkflowInstanceVariable: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **workflowId** | **string**| Идентификатор процесса | 
 **instanceId** | **string**| Идентификатор экземпляра процесса | 
 **workflowInstanceVariable** | [**WorkflowInstanceVariable**](WorkflowInstanceVariable.md)| Переменная процесса | 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="getengineworkflowinstancevariablevalue"></a>
# **GetEngineWorkflowInstanceVariableValue**
> WorkflowInstanceVariable GetEngineWorkflowInstanceVariableValue (string workflowId, string instanceId, string variableId)

Получение значения переменной экземпляра процесса.

### Example
```csharp
using System;
using System.Diagnostics;
using BPMSAPI.Client.Api;
using BPMSAPI.Client.Client;
using BPMSAPI.Client.Model;

namespace Example
{
    public class GetEngineWorkflowInstanceVariableValueExample
    {
        public void main()
        {
            var apiInstance = new WorkflowInstanceVariablesApi();
            var workflowId = workflowId_example;  // string | Идентификатор процесса
            var instanceId = instanceId_example;  // string | Идентификатор экземпляра процесса
            var variableId = variableId_example;  // string | Идентификатор переменной экземпляра процесса

            try
            {
                // Получение значения переменной экземпляра процесса.
                WorkflowInstanceVariable result = apiInstance.GetEngineWorkflowInstanceVariableValue(workflowId, instanceId, variableId);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling WorkflowInstanceVariablesApi.GetEngineWorkflowInstanceVariableValue: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **workflowId** | **string**| Идентификатор процесса | 
 **instanceId** | **string**| Идентификатор экземпляра процесса | 
 **variableId** | **string**| Идентификатор переменной экземпляра процесса | 

### Return type

[**WorkflowInstanceVariable**](WorkflowInstanceVariable.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="getengineworkflowinstancevariables"></a>
# **GetEngineWorkflowInstanceVariables**
> WorkflowInstanceVariableList GetEngineWorkflowInstanceVariables (string workflowId, string instanceId, int? pageNum = null, int? pageSize = null)

Получение списка переменных экземпляра процесса.

### Example
```csharp
using System;
using System.Diagnostics;
using BPMSAPI.Client.Api;
using BPMSAPI.Client.Client;
using BPMSAPI.Client.Model;

namespace Example
{
    public class GetEngineWorkflowInstanceVariablesExample
    {
        public void main()
        {
            var apiInstance = new WorkflowInstanceVariablesApi();
            var workflowId = workflowId_example;  // string | Идентификатор процесса
            var instanceId = instanceId_example;  // string | Идентификатор экземпляра процесса
            var pageNum = 56;  // int? | Номер страницы (optional) 
            var pageSize = 56;  // int? | Размер страницы (optional) 

            try
            {
                // Получение списка переменных экземпляра процесса.
                WorkflowInstanceVariableList result = apiInstance.GetEngineWorkflowInstanceVariables(workflowId, instanceId, pageNum, pageSize);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling WorkflowInstanceVariablesApi.GetEngineWorkflowInstanceVariables: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **workflowId** | **string**| Идентификатор процесса | 
 **instanceId** | **string**| Идентификатор экземпляра процесса | 
 **pageNum** | **int?**| Номер страницы | [optional] 
 **pageSize** | **int?**| Размер страницы | [optional] 

### Return type

[**WorkflowInstanceVariableList**](WorkflowInstanceVariableList.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="setengineworkflowinstancevariable"></a>
# **SetEngineWorkflowInstanceVariable**
> void SetEngineWorkflowInstanceVariable (string workflowId, string instanceId, string variableId, string workflowInstanceValiableValue)

Изменение переменной в экземпляре процесса.

### Example
```csharp
using System;
using System.Diagnostics;
using BPMSAPI.Client.Api;
using BPMSAPI.Client.Client;
using BPMSAPI.Client.Model;

namespace Example
{
    public class SetEngineWorkflowInstanceVariableExample
    {
        public void main()
        {
            var apiInstance = new WorkflowInstanceVariablesApi();
            var workflowId = workflowId_example;  // string | Идентификатор процесса
            var instanceId = instanceId_example;  // string | Идентификатор экземпляра процесса
            var variableId = variableId_example;  // string | Идентификатор переменной экземпляра процесса
            var workflowInstanceValiableValue = workflowInstanceValiableValue_example;  // string | Значение переменной процесса

            try
            {
                // Изменение переменной в экземпляре процесса.
                apiInstance.SetEngineWorkflowInstanceVariable(workflowId, instanceId, variableId, workflowInstanceValiableValue);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling WorkflowInstanceVariablesApi.SetEngineWorkflowInstanceVariable: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **workflowId** | **string**| Идентификатор процесса | 
 **instanceId** | **string**| Идентификатор экземпляра процесса | 
 **variableId** | **string**| Идентификатор переменной экземпляра процесса | 
 **workflowInstanceValiableValue** | **string**| Значение переменной процесса | 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

