# BPMSAPI.Client.Api.ActivitiesApi

All URIs are relative to *http://localhost:5000/api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**GetEngineWorkflowInstanceActivities**](ActivitiesApi.md#getengineworkflowinstanceactivities) | **GET** /workflows/{workflowId}/instances/{instanceId}/activities | Получение списка активностей экземпляра процесса.
[**GetEngineWorkflowInstanceActivity**](ActivitiesApi.md#getengineworkflowinstanceactivity) | **GET** /workflows/{workflowId}/instances/{instanceId}/activities/{activityId} | Получение активности по идентификатору


<a name="getengineworkflowinstanceactivities"></a>
# **GetEngineWorkflowInstanceActivities**
> ActivityList GetEngineWorkflowInstanceActivities (string workflowId, string instanceId, int? pageNum = null, int? pageSize = null)

Получение списка активностей экземпляра процесса.

### Example
```csharp
using System;
using System.Diagnostics;
using BPMSAPI.Client.Api;
using BPMSAPI.Client.Client;
using BPMSAPI.Client.Model;

namespace Example
{
    public class GetEngineWorkflowInstanceActivitiesExample
    {
        public void main()
        {
            var apiInstance = new ActivitiesApi();
            var workflowId = workflowId_example;  // string | Идентификатор процесса
            var instanceId = instanceId_example;  // string | Идентификатор экземпляра процесса
            var pageNum = 56;  // int? | Номер страницы (optional) 
            var pageSize = 56;  // int? | Размер страницы (optional) 

            try
            {
                // Получение списка активностей экземпляра процесса.
                ActivityList result = apiInstance.GetEngineWorkflowInstanceActivities(workflowId, instanceId, pageNum, pageSize);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling ActivitiesApi.GetEngineWorkflowInstanceActivities: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **workflowId** | **string**| Идентификатор процесса | 
 **instanceId** | **string**| Идентификатор экземпляра процесса | 
 **pageNum** | **int?**| Номер страницы | [optional] 
 **pageSize** | **int?**| Размер страницы | [optional] 

### Return type

[**ActivityList**](ActivityList.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="getengineworkflowinstanceactivity"></a>
# **GetEngineWorkflowInstanceActivity**
> ActivityFull GetEngineWorkflowInstanceActivity (string activityId, string workflowId, string instanceId)

Получение активности по идентификатору

### Example
```csharp
using System;
using System.Diagnostics;
using BPMSAPI.Client.Api;
using BPMSAPI.Client.Client;
using BPMSAPI.Client.Model;

namespace Example
{
    public class GetEngineWorkflowInstanceActivityExample
    {
        public void main()
        {
            var apiInstance = new ActivitiesApi();
            var activityId = activityId_example;  // string | Идентификатор активности
            var workflowId = workflowId_example;  // string | Идентификатор процесса
            var instanceId = instanceId_example;  // string | Идентификатор экземпляра процесса

            try
            {
                // Получение активности по идентификатору
                ActivityFull result = apiInstance.GetEngineWorkflowInstanceActivity(activityId, workflowId, instanceId);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling ActivitiesApi.GetEngineWorkflowInstanceActivity: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **activityId** | **string**| Идентификатор активности | 
 **workflowId** | **string**| Идентификатор процесса | 
 **instanceId** | **string**| Идентификатор экземпляра процесса | 

### Return type

[**ActivityFull**](ActivityFull.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

