# BPMSAPI.Client.Api.ActionsApi

All URIs are relative to *http://localhost:5000/api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**CompleteActivity**](ActionsApi.md#completeactivity) | **POST** /workflows/{workflowId}/instances/{instanceId}/activities/{activityId}/complete | Завершение задачи с результатом.
[**StartWorkflowInstance**](ActionsApi.md#startworkflowinstance) | **POST** /workflows/{workflowId}/instances/start | Запуск экземпляра процесса
[**StopWorkflowInstance**](ActionsApi.md#stopworkflowinstance) | **POST** /workflows/{workflowId}/instances/{instanceId}/abort | Остановка экземпляра процесса.


<a name="completeactivity"></a>
# **CompleteActivity**
> void CompleteActivity (string activityId, List<DictionaryItem> activityResults, string workflowId, string instanceId)

Завершение задачи с результатом.

### Example
```csharp
using System;
using System.Diagnostics;
using BPMSAPI.Client.Api;
using BPMSAPI.Client.Client;
using BPMSAPI.Client.Model;

namespace Example
{
    public class CompleteActivityExample
    {
        public void main()
        {
            var apiInstance = new ActionsApi();
            var activityId = activityId_example;  // string | Идентификатор активности
            var activityResults = new List<DictionaryItem>(); // List<DictionaryItem> | Результаты активаности
            var workflowId = workflowId_example;  // string | Идентификатор процесса
            var instanceId = instanceId_example;  // string | Идентификатор экземпляра процесса

            try
            {
                // Завершение задачи с результатом.
                apiInstance.CompleteActivity(activityId, activityResults, workflowId, instanceId);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling ActionsApi.CompleteActivity: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **activityId** | **string**| Идентификатор активности | 
 **activityResults** | [**List&lt;DictionaryItem&gt;**](DictionaryItem.md)| Результаты активаности | 
 **workflowId** | **string**| Идентификатор процесса | 
 **instanceId** | **string**| Идентификатор экземпляра процесса | 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="startworkflowinstance"></a>
# **StartWorkflowInstance**
> WorkflowInstance StartWorkflowInstance (string workflowId, List<WorkflowInstanceVariable> workflowInstanceVariables)

Запуск экземпляра процесса

### Example
```csharp
using System;
using System.Diagnostics;
using BPMSAPI.Client.Api;
using BPMSAPI.Client.Client;
using BPMSAPI.Client.Model;

namespace Example
{
    public class StartWorkflowInstanceExample
    {
        public void main()
        {
            var apiInstance = new ActionsApi();
            var workflowId = workflowId_example;  // string | Идентификатор процесса
            var workflowInstanceVariables = new List<WorkflowInstanceVariable>(); // List<WorkflowInstanceVariable> | Переменные процесса

            try
            {
                // Запуск экземпляра процесса
                WorkflowInstance result = apiInstance.StartWorkflowInstance(workflowId, workflowInstanceVariables);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling ActionsApi.StartWorkflowInstance: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **workflowId** | **string**| Идентификатор процесса | 
 **workflowInstanceVariables** | [**List&lt;WorkflowInstanceVariable&gt;**](WorkflowInstanceVariable.md)| Переменные процесса | 

### Return type

[**WorkflowInstance**](WorkflowInstance.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="stopworkflowinstance"></a>
# **StopWorkflowInstance**
> void StopWorkflowInstance (string workflowId, string instanceId)

Остановка экземпляра процесса.

### Example
```csharp
using System;
using System.Diagnostics;
using BPMSAPI.Client.Api;
using BPMSAPI.Client.Client;
using BPMSAPI.Client.Model;

namespace Example
{
    public class StopWorkflowInstanceExample
    {
        public void main()
        {
            var apiInstance = new ActionsApi();
            var workflowId = workflowId_example;  // string | Идентификатор процесса
            var instanceId = instanceId_example;  // string | Идентификатор экземпляра процесса

            try
            {
                // Остановка экземпляра процесса.
                apiInstance.StopWorkflowInstance(workflowId, instanceId);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling ActionsApi.StopWorkflowInstance: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **workflowId** | **string**| Идентификатор процесса | 
 **instanceId** | **string**| Идентификатор экземпляра процесса | 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

