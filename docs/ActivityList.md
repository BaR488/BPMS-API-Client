# BPMSAPI.Client.Model.ActivityList
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Meta** | [**BaseListMeta**](BaseListMeta.md) |  | 
**Items** | [**List&lt;Activity&gt;**](Activity.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

