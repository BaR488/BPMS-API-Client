# BPMSAPI.Client.Model.BaseListMeta
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**PageNum** | **int?** |  | [default to 1]
**PageSize** | **int?** |  | [optional] 
**TotalPages** | **int?** |  | [default to 1]
**TotalRecordCount** | **int?** |  | [default to 0]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

