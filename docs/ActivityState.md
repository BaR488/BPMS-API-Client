# BPMSAPI.Client.Model.ActivityState
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ActivityStateValue** | **string** |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

