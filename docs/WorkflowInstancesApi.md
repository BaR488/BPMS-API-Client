# BPMSAPI.Client.Api.WorkflowInstancesApi

All URIs are relative to *http://localhost:5000/api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**GetEngineWorkflowInstance**](WorkflowInstancesApi.md#getengineworkflowinstance) | **GET** /workflows/{workflowId}/instances/{instanceId} | Получение экземпляра процесса.
[**GetEngineWorkflowInstances**](WorkflowInstancesApi.md#getengineworkflowinstances) | **GET** /workflows/{workflowId}/instances | Получение списка экземпляров процессов.


<a name="getengineworkflowinstance"></a>
# **GetEngineWorkflowInstance**
> WorkflowInstanceFull GetEngineWorkflowInstance (string workflowId, string instanceId)

Получение экземпляра процесса.

### Example
```csharp
using System;
using System.Diagnostics;
using BPMSAPI.Client.Api;
using BPMSAPI.Client.Client;
using BPMSAPI.Client.Model;

namespace Example
{
    public class GetEngineWorkflowInstanceExample
    {
        public void main()
        {
            var apiInstance = new WorkflowInstancesApi();
            var workflowId = workflowId_example;  // string | Идентификатор процесса
            var instanceId = instanceId_example;  // string | Идентификатор экземпляра процесса

            try
            {
                // Получение экземпляра процесса.
                WorkflowInstanceFull result = apiInstance.GetEngineWorkflowInstance(workflowId, instanceId);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling WorkflowInstancesApi.GetEngineWorkflowInstance: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **workflowId** | **string**| Идентификатор процесса | 
 **instanceId** | **string**| Идентификатор экземпляра процесса | 

### Return type

[**WorkflowInstanceFull**](WorkflowInstanceFull.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="getengineworkflowinstances"></a>
# **GetEngineWorkflowInstances**
> WorkflowInstanceList GetEngineWorkflowInstances (string workflowId, int? pageNum = null, int? pageSize = null)

Получение списка экземпляров процессов.

### Example
```csharp
using System;
using System.Diagnostics;
using BPMSAPI.Client.Api;
using BPMSAPI.Client.Client;
using BPMSAPI.Client.Model;

namespace Example
{
    public class GetEngineWorkflowInstancesExample
    {
        public void main()
        {
            var apiInstance = new WorkflowInstancesApi();
            var workflowId = workflowId_example;  // string | Идентификатор процесса
            var pageNum = 56;  // int? | Номер страницы (optional) 
            var pageSize = 56;  // int? | Размер страницы (optional) 

            try
            {
                // Получение списка экземпляров процессов.
                WorkflowInstanceList result = apiInstance.GetEngineWorkflowInstances(workflowId, pageNum, pageSize);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling WorkflowInstancesApi.GetEngineWorkflowInstances: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **workflowId** | **string**| Идентификатор процесса | 
 **pageNum** | **int?**| Номер страницы | [optional] 
 **pageSize** | **int?**| Размер страницы | [optional] 

### Return type

[**WorkflowInstanceList**](WorkflowInstanceList.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

