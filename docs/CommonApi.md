# BPMSAPI.Client.Api.CommonApi

All URIs are relative to *http://localhost:5000/api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**GetBPMSInfo**](CommonApi.md#getbpmsinfo) | **GET** /info | Получить информацию о СУБП.


<a name="getbpmsinfo"></a>
# **GetBPMSInfo**
> BPMSInfo GetBPMSInfo ()

Получить информацию о СУБП.

### Example
```csharp
using System;
using System.Diagnostics;
using BPMSAPI.Client.Api;
using BPMSAPI.Client.Client;
using BPMSAPI.Client.Model;

namespace Example
{
    public class GetBPMSInfoExample
    {
        public void main()
        {
            var apiInstance = new CommonApi();

            try
            {
                // Получить информацию о СУБП.
                BPMSInfo result = apiInstance.GetBPMSInfo();
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling CommonApi.GetBPMSInfo: " + e.Message );
            }
        }
    }
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**BPMSInfo**](BPMSInfo.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

