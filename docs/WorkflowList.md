# BPMSAPI.Client.Model.WorkflowList
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Meta** | [**BaseListMeta**](BaseListMeta.md) |  | 
**Items** | [**List&lt;Workflow&gt;**](Workflow.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

