# BPMSAPI.Client.Model.WorkflowInstanceVariableList
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Meta** | [**BaseListMeta**](BaseListMeta.md) |  | 
**Items** | [**List&lt;WorkflowInstanceVariable&gt;**](WorkflowInstanceVariable.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

