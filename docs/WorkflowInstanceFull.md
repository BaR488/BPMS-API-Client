# BPMSAPI.Client.Model.WorkflowInstanceFull
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | **string** |  | 
**WorkflowId** | **string** |  | 
**State** | [**WorkflowInstanceState**](WorkflowInstanceState.md) |  | 
**Variebles** | [**List&lt;WorkflowInstanceVariable&gt;**](WorkflowInstanceVariable.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

