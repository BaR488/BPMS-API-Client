# BPMSAPI.Client.Api.WorkflowsApi

All URIs are relative to *http://localhost:5000/api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**GetEngineWorkflow**](WorkflowsApi.md#getengineworkflow) | **GET** /workflows/{workflowId} | Получение конкретного процесса.
[**GetEngineWorkflows**](WorkflowsApi.md#getengineworkflows) | **GET** /workflows | Получение списка доступных процессов СУБП


<a name="getengineworkflow"></a>
# **GetEngineWorkflow**
> Workflow GetEngineWorkflow (string workflowId)

Получение конкретного процесса.

### Example
```csharp
using System;
using System.Diagnostics;
using BPMSAPI.Client.Api;
using BPMSAPI.Client.Client;
using BPMSAPI.Client.Model;

namespace Example
{
    public class GetEngineWorkflowExample
    {
        public void main()
        {
            var apiInstance = new WorkflowsApi();
            var workflowId = workflowId_example;  // string | Идентификатор процесса

            try
            {
                // Получение конкретного процесса.
                Workflow result = apiInstance.GetEngineWorkflow(workflowId);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling WorkflowsApi.GetEngineWorkflow: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **workflowId** | **string**| Идентификатор процесса | 

### Return type

[**Workflow**](Workflow.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="getengineworkflows"></a>
# **GetEngineWorkflows**
> WorkflowList GetEngineWorkflows (int? pageNum = null, int? pageSize = null)

Получение списка доступных процессов СУБП

### Example
```csharp
using System;
using System.Diagnostics;
using BPMSAPI.Client.Api;
using BPMSAPI.Client.Client;
using BPMSAPI.Client.Model;

namespace Example
{
    public class GetEngineWorkflowsExample
    {
        public void main()
        {
            var apiInstance = new WorkflowsApi();
            var pageNum = 56;  // int? | Номер страницы (optional) 
            var pageSize = 56;  // int? | Размер страницы (optional) 

            try
            {
                // Получение списка доступных процессов СУБП
                WorkflowList result = apiInstance.GetEngineWorkflows(pageNum, pageSize);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling WorkflowsApi.GetEngineWorkflows: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **pageNum** | **int?**| Номер страницы | [optional] 
 **pageSize** | **int?**| Размер страницы | [optional] 

### Return type

[**WorkflowList**](WorkflowList.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

