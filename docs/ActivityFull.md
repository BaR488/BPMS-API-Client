# BPMSAPI.Client.Model.ActivityFull
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | **string** |  | 
**Name** | **string** |  | [optional] 
**ProcessInstanceId** | **string** |  | 
**State** | [**ActivityState**](ActivityState.md) |  | [optional] 
**Parameters** | [**List&lt;DictionaryItem&gt;**](DictionaryItem.md) |  | [optional] 
**Results** | [**List&lt;DictionaryItem&gt;**](DictionaryItem.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

